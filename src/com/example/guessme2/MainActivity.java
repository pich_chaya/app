package com.example.guessme2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
implements OnClickListener{
	
	int totalScore;
	String ansString = "";
	String realAns;
	ArrayList<String> ansArray; // array of collecting real answer
	ArrayList<String> hintArray;
	int level=0; // collect level
	int score = 0; // collect score

	int state;
	int [] picArray = {R.drawable.pic4,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic7,
			R.drawable.pic9,R.drawable.pic16,R.drawable.pic23,R.drawable.pic24,R.drawable.pic25,
			R.drawable.pic26,R.drawable.pic28,R.drawable.pic30,R.drawable.pic33,R.drawable.pic36,
			R.drawable.pic37,R.drawable.pic38,R.drawable.pic39,R.drawable.pic40,R.drawable.pic42,
			R.drawable.pic43,R.drawable.eplaces};
	// array of collecting picture
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		int state;
		
		 loadSavedPreferences();
	
		Button b1 = (Button)findViewById(R.id.submit);
		ImageButton b2 = (ImageButton)findViewById(R.id.settingBT);
		ImageButton b3 = (ImageButton)findViewById(R.id.hintBT);
		ImageButton b4 = (ImageButton)findViewById(R.id.skipBT);
		
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        
      
        
        Intent i = this.getIntent();
        score = i.getIntExtra("score",0);
        loadSavedPreferences();
        if(i.hasExtra("state"))
		{
        
        	state = i.getIntExtra("state",-1);
        	level = state;
        	
        	TextView lv = (TextView)findViewById(R.id.level);
			lv.setText("Level " + level);
			
			TextView sc = (TextView)findViewById(R.id.score);
			sc.setText("Score : " + score);
			
        	ImageView img= (ImageView) findViewById(R.id.image);
			img.setImageResource(picArray[level]);
        	
		}
        
	
	}
	
	
	private void loadSavedPreferences() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		score =sharedPreferences.getInt("score", score);
		TextView sc = (TextView)findViewById(R.id.score);
		sc.setText("Score : " + score);
		
	}
	
	private void savePreferences(String key, int value) {SharedPreferences sharedPreferences = PreferenceManager
.getDefaultSharedPreferences(this);

	Editor editor = sharedPreferences.edit();
	editor.putInt(key, value);
		editor.commit();
	}

	
	public void onBackPressed() {
		
		Intent i = new Intent(getApplicationContext(), State.class);
		i.putExtra("score", score);
		this.setResult(RESULT_OK, i);
		this.finish();
		startActivity(i);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		
		
		state = level;
		// TODO Auto-generated method stub
		int id = v.getId();
       	
		final EditText ans = (EditText)findViewById(R.id.answer);
		TextView sc = (TextView)findViewById(R.id.score);
		//TextView lv = (TextView)findViewById(R.id.level);
		
		switch(id){
		case R.id.submit:
			ansString = ans.getText().toString();   // input answer
			ansString = ansString.toLowerCase();	// convert answer to lowercase to check
	        
			// array of the correct answer
			ansArray = new ArrayList<String>();
	        ansArray.add("wat phra kaew"); 		// [0] level 1
	        ansArray.add("big ben");			// [1] level 2
	        ansArray.add("colosseum"); 			// [2] level 3
	        ansArray.add("eiffel tower");		// [3] level 4
	        ansArray.add("the great piramid of giza");	// [4] level 5
	        ansArray.add("petronas towers");
	        ansArray.add("machu picchu");
	        ansArray.add("merlion");
	        ansArray.add("mount rushmore");
	        ansArray.add("notre dame");			// level 10
	        ansArray.add("petra");
	        ansArray.add("christ the redeemer");
	        ansArray.add("neuschwanstein");
	        ansArray.add("statue of liberty");
	        ansArray.add("sydney opera house");	// level 15
	        ansArray.add("taj mahal");
	        ansArray.add("the great wall");
	        ansArray.add("the leaning tower of pisa");
	        ansArray.add("the louvre");
	        ansArray.add("tokyo tower");		// level 20
	        
	        realAns = ansArray.get(level-1); // real answer from array
	        
	        // check input = answer or not
	        // right answer
	        if(ansString.equals(realAns)){ 
	            
	        	level+=1;
	        	final int levelfinal = level;
	        	
	        	score+=5;
	        	sc.setText("Score : " + score);
	        	
	        	// check if it out of level -> go to new class
	        	if(levelfinal == 21){
		        	AlertDialog dialog = new AlertDialog.Builder(this).create(); 
		        	dialog.setTitle("Congratulation"); 
		        	dialog.setMessage("You finished our level :)"); // *** Insert Description here
		        	dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
		        			new DialogInterface.OnClickListener() {
		        		
		        		// when click on dialog
		        		@Override
		        		public void onClick(DialogInterface dialog, int which) {
		        			
		        			Intent i = new Intent(getApplicationContext(),Congrat.class);
		        			startActivity(i);
		        			
		        		}
		        	} );
		        	
		        	dialog.show();
		        	
	        	// level remaining
	        	}else{
	        		AlertDialog dialog = new AlertDialog.Builder(this).create(); 
		        	dialog.setTitle("Correct"); 
		        	dialog.setMessage("Go to next Level :)"); // *** Insert Description here
		        	dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Next",
		        			new DialogInterface.OnClickListener() {
		        		
		        		// when click on dialog
		        		@Override
		        		public void onClick(DialogInterface dialog, int which) {
		        			// variables have to be final -_-"
		        			ImageView img= (ImageView) findViewById(R.id.image);
		        			img.setImageResource(picArray[levelfinal]);
		        			
		        			TextView lv = (TextView)findViewById(R.id.level);
		        			lv.setText("Level " + levelfinal);
		        			
		        			clear(ans);
		        			
		        		}
		        	} );
		        	
		        	dialog.show();
	        	}
	        	savePreferences("totalscore", score);	
	        // wrong answer
	        }else{
	        	AlertDialog dialog = new AlertDialog.Builder(this).create(); 
	        	dialog.setTitle("Wrong Answer"); 
	        	dialog.setMessage("Please try again..."); 
	        	dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
	        			new DialogInterface.OnClickListener() {
	        	@Override
	        	public void onClick(DialogInterface dialog, int which) {
	        		clear(ans);
	        	}
	        		} );
	        	dialog.show();
	        }
	        break;
	    
		case R.id.settingBT:
			Intent i = new Intent(getApplicationContext(),Setting.class);
			i.putExtra("state", state);
			i.putExtra("score", score);
			this.setResult(RESULT_OK, i);
			this.finish();
			startActivity(i);
			savePreferences("totalscore", score);
			break;
		
		case R.id.hintBT:
			score-=2;
        	sc.setText("Score : " + score);
        	
        	hintArray = new ArrayList<String>();
 /* 0 */    hintArray.add("It's the most popular temple in Thailand.");
        	hintArray.add("The great bell of the clock at the north end of the Palace of Westminster in London.");
        	hintArray.add("It was the largest amphitheatre of the Roman Empire.");
        	hintArray.add("It's a global cultural icon of France and one of the most recognisable structures in the world.");
        	hintArray.add("T__ G____ P______ O_ G___,The oldest and largest of the three pyramids in the Giza Necropolis.");
 /* 5 */	hintArray.add("They are twin skyscrapers in Kuala Lumpur, Malaysia.");
 			hintArray.add("It is a 15th-century Inca site located in Peru.");
 			hintArray.add("It is a mythical creature with the head of a lion and the body of a fish, used as a mascot and national personification of Singapore.");
 			hintArray.add("It is a sculpture carved into the granite face of four United States presidents.");
 			hintArray.add("People called 'Our Lady of Paris'.");
 /* 10 */	hintArray.add("The Rose City");
 			hintArray.add("It is a statue of Jesus Christ in Rio de Janeiro, Brazil.");
		 	hintArray.add("It's a nineteenth-century Romanesque Revival palace on a rugged hill above the village of Hohenschwangau near Füssen in southwest Bavaria, Germany.");
			hintArray.add("This statue is an icon of freedom and of the United States: a welcoming signal to immigrants arriving from abroad.");
			hintArray.add("It is a multi-venue performing arts centre in Sydney, New South Wales, Australia.");
/* 15 */	hintArray.add("It is widely recognized as 'the jewel of Muslim art in India and one of the universally admired masterpieces of the world's heritage'.");
 			hintArray.add("Borders of China in part to protect the Chinese Empire or its prototypical states against intrusions by various nomadic groups.");
		 	hintArray.add("It is the campanile, or freestanding bell tower, of the cathedral of the Italian city of Pisa.");
			hintArray.add("It is one of the world's largest museums and a historic monument. A central landmark of Paris, France.");
			hintArray.add("It is the second-tallest structure in Japan. The structure is an Eiffel Tower-inspired lattice tower.");
 			
        	String hintSt = hintArray.get(level-1);
			
			AlertDialog dialog = new AlertDialog.Builder(this).create(); 
        	dialog.setTitle("Hint"); 
        	dialog.setMessage(hintSt); 
        	dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
        			new DialogInterface.OnClickListener() {
        		@Override
        		public void onClick(DialogInterface dialog, int which) {}
        	} );
        	
        	dialog.show();
        	savePreferences("totalscore", score);
			break;
		
		case R.id.skipBT:
			// if skip at level 20 -> go back to select level page
			if(level==20){ 
				Toast t2 = Toast.makeText(this, "No more level!!", 
						Toast.LENGTH_SHORT);
				t2.show();
				Intent i3 = new Intent(getApplicationContext(),State.class);
				startActivity(i3);
			}else{
				level++;
			
				ImageView img= (ImageView) findViewById(R.id.image);
				img.setImageResource(picArray[level]);
				
				TextView lv = (TextView)findViewById(R.id.level);
				lv.setText("Level " + (level));
				
				score-=2;
	        	sc.setText("Score : " + score);
				
				clear(ans);	
			}
			savePreferences("totalscore", score);
			break;
		} 
		
		
		//finish();

	} 

	
	public void clear(View v) {
		EditText ans = (EditText)findViewById(R.id.answer);
	    ans.setText("");

	 }

}