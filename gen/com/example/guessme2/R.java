/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.guessme2;

public final class R {
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarButtonStyle=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int buttonBarStyle=0x7f010000;
    }
    public static final class color {
        public static final int black_overlay=0x7f040000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int btm=0x7f020000;
        public static final int eplaces=0x7f020001;
        public static final int hint=0x7f020002;
        public static final int ic_launcher=0x7f020003;
        public static final int introduction=0x7f020004;
        public static final int main=0x7f020005;
        public static final int modefruit=0x7f020006;
        public static final int modeplace=0x7f020007;
        public static final int pic16=0x7f020008;
        public static final int pic23=0x7f020009;
        public static final int pic24=0x7f02000a;
        public static final int pic25=0x7f02000b;
        public static final int pic26=0x7f02000c;
        public static final int pic28=0x7f02000d;
        public static final int pic30=0x7f02000e;
        public static final int pic33=0x7f02000f;
        public static final int pic36=0x7f020010;
        public static final int pic37=0x7f020011;
        public static final int pic38=0x7f020012;
        public static final int pic39=0x7f020013;
        public static final int pic4=0x7f020014;
        public static final int pic40=0x7f020015;
        public static final int pic42=0x7f020016;
        public static final int pic43=0x7f020017;
        public static final int pic5=0x7f020018;
        public static final int pic6=0x7f020019;
        public static final int pic7=0x7f02001a;
        public static final int pic9=0x7f02001b;
        public static final int places2=0x7f02001c;
        public static final int rs=0x7f02001d;
        public static final int setting=0x7f02001e;
        public static final int sp=0x7f02001f;
        public static final int st1=0x7f020020;
        public static final int st2=0x7f020021;
    }
    public static final class id {
        public static final int TextView01=0x7f090027;
        public static final int TextView02=0x7f090028;
        public static final int TextView03=0x7f090029;
        public static final int TextView04=0x7f09002a;
        public static final int TextView05=0x7f09002b;
        public static final int TextView06=0x7f09002c;
        public static final int TextView07=0x7f09002d;
        public static final int TextView08=0x7f09002e;
        public static final int TextView09=0x7f09002f;
        public static final int TextView10=0x7f090030;
        public static final int TextView11=0x7f090031;
        public static final int TextView12=0x7f090032;
        public static final int TextView13=0x7f090033;
        public static final int TextView14=0x7f090034;
        public static final int TextView15=0x7f090035;
        public static final int TextView16=0x7f090036;
        public static final int TextView17=0x7f090037;
        public static final int TextView18=0x7f090038;
        public static final int TextView19=0x7f090039;
        public static final int answer=0x7f090009;
        public static final int backBT=0x7f09000f;
        public static final int currentScore=0x7f09003a;
        public static final int dummy_button=0x7f090003;
        public static final int fullscreen_content=0x7f090000;
        public static final int fullscreen_content_controls=0x7f090002;
        public static final int hintBT=0x7f09000d;
        public static final int image=0x7f090008;
        public static final int imageView1=0x7f090001;
        public static final int l01=0x7f090012;
        public static final int l02=0x7f09001e;
        public static final int l03=0x7f09001d;
        public static final int l04=0x7f090022;
        public static final int l05=0x7f09001f;
        public static final int l06=0x7f090013;
        public static final int l07=0x7f090015;
        public static final int l08=0x7f090014;
        public static final int l09=0x7f090023;
        public static final int l10=0x7f090017;
        public static final int l11=0x7f09001a;
        public static final int l12=0x7f090020;
        public static final int l13=0x7f090021;
        public static final int l14=0x7f090018;
        public static final int l15=0x7f090016;
        public static final int l16=0x7f090019;
        public static final int l17=0x7f090025;
        public static final int l18=0x7f090024;
        public static final int l19=0x7f09001c;
        public static final int l20=0x7f09001b;
        public static final int level=0x7f090007;
        public static final int m_fruit=0x7f090004;
        public static final int m_place=0x7f090005;
        public static final int okBT=0x7f090010;
        public static final int resetBT=0x7f09000e;
        public static final int score=0x7f090006;
        public static final int settingBT=0x7f09000c;
        public static final int skipBT=0x7f09000b;
        public static final int submit=0x7f09000a;
        public static final int textView1=0x7f090011;
        public static final int textView2=0x7f090026;
    }
    public static final class layout {
        public static final int activity_congrat=0x7f030000;
        public static final int activity_fullscreen=0x7f030001;
        public static final int activity_main=0x7f030002;
        public static final int activity_setting=0x7f030003;
        public static final int activity_state=0x7f030004;
    }
    public static final class menu {
        public static final int congrat=0x7f080000;
        public static final int main=0x7f080001;
        public static final int setting=0x7f080002;
        public static final int state=0x7f080003;
    }
    public static final class string {
        public static final int action_settings=0x7f060004;
        public static final int answer=0x7f060008;
        public static final int app_name=0x7f060000;
        public static final int back=0x7f060012;
        public static final int dummy_button=0x7f060001;
        public static final int dummy_content=0x7f060002;
        public static final int hello_world=0x7f060005;
        public static final int jig1=0x7f06000b;
        public static final int jig2=0x7f06000a;
        public static final int jig3=0x7f06000c;
        public static final int jig4=0x7f06000d;
        public static final int level=0x7f060006;
        public static final int ok=0x7f060013;
        public static final int reset=0x7f060011;
        public static final int score=0x7f060007;
        public static final int submit=0x7f060009;
        public static final int title_activity_congrat=0x7f06000e;
        public static final int title_activity_main=0x7f060003;
        public static final int title_activity_setting=0x7f060010;
        public static final int title_activity_state=0x7f06000f;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
        public static final int ButtonBar=0x7f070003;
        public static final int ButtonBarButton=0x7f070004;
        public static final int FullscreenActionBarStyle=0x7f070005;
        public static final int FullscreenTheme=0x7f070002;
    }
    public static final class styleable {
        /** 
         Declare custom theme attributes that allow changing which styles are
         used for button bars depending on the API level.
         ?android:attr/buttonBarStyle is new as of API 11 so this is
         necessary to support previous API levels.
    
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarButtonStyle com.example.guessme2:buttonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_buttonBarStyle com.example.guessme2:buttonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_buttonBarButtonStyle
           @see #ButtonBarContainerTheme_buttonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.example.guessme2.R.attr#buttonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.guessme2:buttonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.example.guessme2.R.attr#buttonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.example.guessme2:buttonBarStyle
        */
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
    };
}
